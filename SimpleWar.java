public class SimpleWar{
	
	
	public static void main(String[] args){
		
		
		Deck gameDeck = new Deck();
		gameDeck.shuffleDeck();
		
		double playerOne = 0;
		double playerTwo = 0;
		double playerOneScore = 0;
		double playerTwoScore = 0;
		int gameRounds = 1;
		
		
		
		
		while(gameDeck.length() > 0){
			Card calculations = gameDeck.drawTopCard();
			System.out.println(calculations);
			playerOne = calculations.calculateScore();
			System.out.println("Player One: " + playerOne);
			calculations = gameDeck.drawTopCard();
			System.out.println(calculations);
			playerTwo = calculations.calculateScore();
			System.out.println("Player Two: " + playerTwo);
			

			if(playerOne > playerTwo){
				
				System.out.println("Player 1 has won the " + gameRounds + " round of Simpler War with a score of " + playerOne);
				playerOneScore += 1;
				gameRounds += 1;
				System.out.println("\n Player One's Current Score: " + playerOneScore + "\n Player Two's Current Score: " + playerTwoScore);
				
			}
			else{
				
				System.out.print("Player 2 has won the " + gameRounds + " round of Simpler War with a score of " + playerTwo);
				playerTwoScore += 1;
				gameRounds += 1;			
				System.out.println("\n------------------\n Player One's Current Score: " + playerOneScore + "\n Player Two's Current Score: " + playerTwoScore);
			}
		}
		
		
		if(playerOneScore > playerTwoScore){
			
			System.out.println("Congratulations Player One, You've won this game of Simple War");
			
			
		}
		else if(playerTwoScore > playerOneScore){
			
			System.out.println("Congratulations Player Two, You've won this game of Simple War");
			
		}
		else{
			
			
			System.out.println("Congratulations Players, you've both tied in this game of Simple War");
			
		}
		
	}
	
	
	
	
	
	
}