import java.util.Random;
public class Deck{
	
	private Card[] cards;
	private int numberOfCards;
	private Random rng;
	
	public Deck(){
		
		this.rng = new Random();
		

		String[] suits = {"Hearts", "Clubs", "Spades", "Diamonds"};
		
		this.numberOfCards = 52;
		this.cards = new Card[this.numberOfCards];
		
		for(int i= 0; i < this.cards.length; i++){
			int currentValue = 0;
			if(suits[0].equals("Hearts")){
				if(0 <= i && i <= 13){
					cards[i] = new Card("Hearts", i);
					//System.out.println(cards[i]);
				}
			}
			if(suits[1].equals("Clubs") && i >= 14 && i < 27){
				currentValue = i - 13;
				if(1 <= currentValue && currentValue <= 13){
					cards[i] = new Card("Clubs", currentValue);
					//System.out.println(cards[i]);
				}				
				
			}
			if(suits[2].equals("Spades") && i >= 27 && i < 40){
				currentValue = i - 26;
				if(1 <= currentValue && currentValue <= 13){
					cards[i] = new Card("Spades", currentValue);
					//System.out.println(cards[i]);
				}				
				
			}
			if(suits[3].equals("Diamonds") && i >= 40){
				currentValue = i - 39;
				if(1 <= currentValue && currentValue <= 13){
					cards[i] = new Card("Diamonds", currentValue);
					//System.out.println(cards[i]);
				}				
				
			}
		}
		
		
		
		
		
	}
	
	
	public int length(){
		return this.numberOfCards;
	}
	
	public Card drawTopCard(){
		
		this.numberOfCards--;
		return this.cards[this.numberOfCards];
		
	}
	
	
	
	public String toString(){
		
		String result = "";
		
		for(int i = 0; i < numberOfCards; i++){
			
			
			result = result + this.cards[i] + "\n";
			
			
		}
		
		return result;
		
	}
	
	
	
	
	public void shuffleDeck(){

	
		this.rng = new Random();

		for(int i = 0; i < this.numberOfCards; i++){
			int value = this.rng.nextInt(52);
			Card storage = this.cards[value];
			this.cards[value] = this.cards[i];
			this.cards[i] = storage;
			
		}
		for(int i = 0; i < this.numberOfCards; i++){
			
			System.out.println(this.cards[i]);
			
		}
		
	}
	
	
	
	
	
	
	
	
	
	
	
}